asgiref==3.2.7
Django==3.0.5
django-extensions==2.2.9
python-dotenv==0.13.0
pytz==2019.3
six==1.14.0
sqlparse==0.3.1
